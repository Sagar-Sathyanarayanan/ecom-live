#!/bin/bash

set -f
string=$AWS_IP
array=(${string//,/ })


for i in "${!array[@]}"; do
  echo "Deploy on EC2 AWS"
  echo "Deploy django ecom app on to the server ${array[i]}"
  ssh ubuntu@${array[i]}  "cd ecom-live && git pull origin main && sudo docker-compose build && docker-compose up -d"
done
